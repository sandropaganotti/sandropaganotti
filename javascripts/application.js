/*var Rules = {
	
	'.post a:click' : function(element){
		alert("ciao");
	}
}
 
Event.observe(window, 'load', function(event){ EventSelectors.start(Rules); });*/
var default_blog_left = 250;
var default_post_left = 250;
var current_mode = 'from_post';

function ritorna(){
	if($('comment_post') != null){
		$('comment_post').remove();
	}
	new Effect.Fade($('ajax_post'),{
		afterFinish: function(obj){
			$('page').morph($('page').magnify_string(5.0) + " left: " + default_blog_left + "px;");
			$('ajax_post').remove();
		}
	});
}

function torna_commenti(){
	new Effect.Fade($('comment_post'),{
		afterFinish: function(obj){
			$('ajax_post').morph($('ajax_post').magnify_string(5.0) + " left: " + default_post_left + "px;");
			$('comment_post').remove();
		}
	});
}


function lascia_un_commento(post_id){
	if($('comment_post') == null){
		
		default_post_left = parseInt($('ajax_post').getStyle('left'));
		new Effect.ScrollTo($('ajax_post'),{
			afterFinish: function(obj){
				$('ajax_post').morph($('ajax_post').magnify_string(0.2),{
					afterFinish: function(obj){
						$('ajax_post').insert({after: new Element('div',{'id':'comment_post'}).update( 
							"<a href='javascript:torna_commenti()' style='padding-left:8px;'>&laquo; clicca per tornare</a>"
						)});
						$('comment_post').setStyle({
							left: 		"" + (parseInt($('ajax_post').getStyle('left')) + $('ajax_post').getWidth() + 20) + "px"
						});
						$('comment_post').insert({bottom: new Element('iframe',{'src':'http://www.sandropaganotti.com/wp-content/themes/zoom/iframe.php?post_id=' + post_id, 'width':'100%','height':'500px', 'scrolling':'no', 'frameborder':'0'})});
						
					}
				});
			}		
		});
	}
}

function richiama_post(url){
	
	if($('ajax_post') == null){
	
	default_blog_left = parseInt($('page').getStyle('left'));
	new Effect.ScrollTo($('page'),{
		afterFinish: function(null_obj){
			$('page').morph($('page').magnify_string(0.2) + " left: 20px;",{
				afterFinish: function(obj){
					new Ajax.Request(url,{
						onSuccess: function(transport) {		
							return_txt = new Element('div').update(transport.responseText)	
								$('page').insert({after: new Element('div',{'id':'ajax_post'}).update( 
								"<a href='javascript:ritorna()'>&laquo; clicca per tornare</a>" +
								return_txt.select(".post")[0].innerHTML
							)});
							$('ajax_post').setStyle({
								left: 		"" + (parseInt($('page').getStyle('left')) + $('page').getWidth() + 20) + "px",
								display:  "none"
							}); 
							new Effect.Appear($('ajax_post'));
						}
					});
				}
			});
		}
	});
	
	}else{
	
	new Effect.Fade($('ajax_post'),{
		afterFinish: function(obj){
		new Ajax.Request(url,{
			onSuccess: function(transport){
				$('ajax_post').update(
					"<a href='javascript:ritorna()'>&laquo; clicca per tornare</a>" +
					new Element('div').update(transport.responseText).select(".post")[0].innerHTML
				);
				new Effect.Appear($('ajax_post'));
			}
		});}
	});
		
	}
	
	
}


Element.addMethods({

  /* Zoom Effect is simply a morph effect on a date
     element witch resize proportionally both width and
     height.
  */
  magnify_string: function(element, sf) {
    dimensions = element.getDimensions();
    font_size  = ( element.getStyle('font-size') == null ? 16 : parseFloat(element.getStyle('font-size')));
    new_width  = ( dimensions.width  > 0 ? dimensions.width  : 1.0)*sf;
    /*new_height = ( dimensions.height > 0 ? dimensions.height : 1.0)*sf;*/
    new_size   = ( font_size > 0         ? font_size         : 1.0)*sf;
    return "width: " + new_width + "px; font-size: " + new_size + "px;";
  },

  percentage: function(image, dims){
	  dimensions = image.getDimensions();
		new_height = (image.height * 100.0) / dims.height;
		new_width  = (image.width * 100.0) / dims.width;
		image.setStyle({
			height: "" + new_height + "%",
			width:  "" + new_width + "%"
		});
	}

    
});

Event.observe(window,'load', function(event){
	
	
	if ($$('.single_post').size() > 0){
			
			current_mode = 'from_post';
			$('page').hide();
			$('page').insert({after: new Element('div',{'id':'ajax_post','style':'display: none;'}).update( 
				"<a href='javascript:ritorna()'>&laquo; clicca per tornare</a>" +
				$('page').select(".post")[0].innerHTML
			)});
			$('ajax_post').setStyle({
				left: 		"194px",
				display:  "none"
			}); 
			$('page').remove();
			new Effect.Appear($('ajax_post'),{
					afterFinish: function(obj){
						new Ajax.Request('/',{
							onSuccess: function(transport){
								$('ajax_post').insert({before: new Element('div').update(transport.responseText).select("#page")[0].setStyle({visibility: 'hidden'})});
								$('page').absolutize();
								$('page').setStyle({height: "auto", marginTop: "0px"});
								$('page').morph($('page').magnify_string(0.2) + " left: 20px;",{
									afterFinish: function(obj){
										$('page').hide();
										$('page').setStyle({visibility: 'visible'});
										Effect.Appear($('page'));
									}
								});
							}
						})
					}
			});
	
	}else{
		
			current_mode = 'from_hp';
			$$('.post a.more-link').each(function(ele){
				ele.href="javascript:richiama_post('" + ele.href + "');"
			});

			/* I'll have to cross all the element and transform 
			   fixed properties into percentage
			*/
			 $('page').absolutize();
			 $('page').setStyle({height: "auto", marginTop: "0px"});
			 $$('img').each(function(ele){ ele.percentage(ele.ancestors()[0].getDimensions()) });
	}
			

});

