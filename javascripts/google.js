google.load("prototype", "1.6.0.2");
google.load('friendconnect', '0.8');

var skin = {};
skin['FONT_FAMILY'] = 'verdana,sans-serif';
skin['BORDER_COLOR'] = '#cccccc';
skin['ENDCAP_BG_COLOR'] = '##D5D6D7';
skin['ENDCAP_TEXT_COLOR'] = '#333333';
skin['ENDCAP_LINK_COLOR'] = '#0000cc';
skin['ALTERNATE_BG_COLOR'] = '#ffffff';
skin['CONTENT_BG_COLOR'] = '#ffffff';
skin['CONTENT_LINK_COLOR'] = '#0000cc';
skin['CONTENT_TEXT_COLOR'] = '#333333';
skin['CONTENT_SECONDARY_LINK_COLOR'] = '#7777cc';
skin['CONTENT_SECONDARY_TEXT_COLOR'] = '#666666';
skin['CONTENT_HEADLINE_COLOR'] = '#333333';
skin['DEFAULT_COMMENT_TEXT'] = '- add your comment here -';
skin['HEADER_TEXT'] = 'Comments';
skin['POSTS_PER_PAGE'] = '15';

// function initialize() {
// 
// 	google.friendconnect.container.setParentUrl('/' /* location of rpc_relay.html and canvas.html */);
// 	google.friendconnect.container.initOpenSocialApi({
// 	  site: '13311066059759894834',
// 	  onload: function(securityToken) { /* your callback, which is passed a security token */ }
// 	});
// 
// }
