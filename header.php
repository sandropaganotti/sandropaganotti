<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>

<head profile="http://gmpg.org/xfn/11">
<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
<meta name="blogcatalog" content="9BC9022312" /> 
<meta name="google-site-verification" content="c7vSNQV9lqn8Fqlm69YOPJjGB1S_bs_oOTkJTWgAwv4" />
<meta property="fb:page_id" content="190257831038998" />

<title><?php bloginfo('name'); ?> <?php if ( is_single() ) { ?> &raquo; Blog Archive <?php } ?> <?php wp_title(); ?></title>

<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" />
<link rel="stylesheet" href="<?php echo(get_theme_root_uri());?>/<?php echo get_option('template');?>/stylesheets/extend.css" type="text/css" media="screen" />
<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSS Feed" href="<?php bloginfo('rss2_url'); ?>" />
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

<script src="<?php echo(get_theme_root_uri());?>/<?php echo get_option('template');?>/javascripts/prototype.js" type="text/javascript"></script>
<script src="<?php echo(get_theme_root_uri());?>/<?php echo get_option('template');?>/javascripts/effects.js" type="text/javascript"></script>
<script src="<?php echo(get_theme_root_uri());?>/<?php echo get_option('template');?>/javascripts/dragdrop.js" type="text/javascript"></script>
<script src="<?php echo(get_theme_root_uri());?>/<?php echo get_option('template');?>/javascripts/controls.js" type="text/javascript"></script>

<script src="<?php echo(get_theme_root_uri());?>/<?php echo get_option('template');?>/javascripts/code_highlighter.js" type="text/javascript"></script>
<script src="<?php echo(get_theme_root_uri());?>/<?php echo get_option('template');?>/javascripts/css.js" type="text/javascript"></script>
<script src="<?php echo(get_theme_root_uri());?>/<?php echo get_option('template');?>/javascripts/html.js" type="text/javascript"></script>
<script src="<?php echo(get_theme_root_uri());?>/<?php echo get_option('template');?>/javascripts/javascript.js" type="text/javascript"></script>
<script src="<?php echo(get_theme_root_uri());?>/<?php echo get_option('template');?>/javascripts/ruby.js" type="text/javascript"></script>

<script src="<?php echo(get_theme_root_uri());?>/<?php echo get_option('template');?>/javascripts/eventselector.js" type="text/javascript"></script>
<script src="<?php echo(get_theme_root_uri());?>/<?php echo get_option('template');?>/javascripts/processing.js" type="text/javascript"></script>
<script src="<?php echo(get_theme_root_uri());?>/<?php echo get_option('template');?>/javascripts/application.js" type="text/javascript"></script>

<?php wp_head(); ?>
</head>
<body>


<div id="page" <?php if(is_single()){ ?> class="single_post" <? }?> >

<!--
<div id="header">
	<div id="headerimg">
		<h1><a href="<?php echo get_option('home'); ?>/"><?php bloginfo('name'); ?></a></h1>
		<div class="description"><?php bloginfo('description'); ?></div>
	</div>
</div>
<hr />
-->

<div id="testata">
	<h1>
		<a href="<?php echo get_option('home'); ?>/"><?php bloginfo('name'); ?></a>
	</h1>
</div>